package ru.test.taxsee.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Dashkevich Makar
 * mcrena@mail.ru  on 08.03.17.
 */

public class Department {

    @SerializedName("ID")
    private Integer id;

    @SerializedName("Name")
    private String name;

    @SerializedName("Departments")
    private List<Department> departments;

    @SerializedName("Employees")
    private List<Employer> employees;

    public boolean expanded = false;

    public Integer getId() {
        if (id == null)
            return 0;
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        if (name == null)
            return "null";
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(List<Department> departments) {
        this.departments = departments;
    }

    public List<Employer> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employer> employees) {
        this.employees = employees;
    }
}
