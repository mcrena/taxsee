package ru.test.taxsee.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Dashkevich Makar
 * mcrena@mail.ru  on 08.03.17.
 */

public class ServerResponse {

    @SerializedName("Message")
    private String message;

    @SerializedName("Success")
    private Boolean success;

    public String getMessage() {
        if (message == null)
            return "null";
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getSuccess() {
        if (success == null)
            return false;
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
