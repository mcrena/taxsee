package ru.test.taxsee.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Dashkevich Makar
 * mcrena@mail.ru  on 08.03.17.
 */

public class Employer implements Serializable {

    @SerializedName("ID")
    private Integer id;

    @SerializedName("Name")
    private String name;

    @SerializedName("Title")
    private String title;

    @SerializedName("Email")
    private String email;

    @SerializedName("Phone")
    private String phone;

    public Integer getId() {
        if (id == null)
            return 0;
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        if (name == null)
            return "null";
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        if (title == null)
            return "null";
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEmail() {
        if (email == null)
            return "null";
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        if (phone == null)
            return "null";
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
