package ru.test.taxsee.main;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import ru.test.taxsee.R;
import ru.test.taxsee.models.Employer;
import ru.test.taxsee.services.ApiDataService;
import ru.test.taxsee.tools.PreferencesHelper;

/**
 * Created by Dashkevich Makar
 * mcrena@mail.ru  on 08.03.17.
 */

public class EmployerFragment extends BaseFragment {
    private ImageView imgEmployer;
    private Employer employer;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.employer_frg, null);
        employer = (Employer) getArguments().getSerializable("employer");
        TextView txtId = (TextView) v.findViewById(R.id.txtEmployerId);
        TextView txtName = (TextView) v.findViewById(R.id.txtEmployerName);
        TextView txtTitle = (TextView) v.findViewById(R.id.txtEmployerTitle);
        TextView txtEmail = (TextView) v.findViewById(R.id.txtEmployerEmail);
        TextView txtPhone = (TextView) v.findViewById(R.id.txtEmployerPhone);
        imgEmployer = (ImageView) v.findViewById(R.id.imgEmployerImage);
        txtId.setText(String.valueOf(employer.getId()));
        txtName.setText(employer.getName());
        txtTitle.setText(employer.getTitle());
        txtEmail.setText(employer.getEmail());
        txtPhone.setText(employer.getPhone());
        v.findViewById(R.id.emailContainer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onEmailClick(employer.getEmail());
            }
        });
        v.findViewById(R.id.phoneContainer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPhoneClick(employer.getPhone());
            }
        });
        return v;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.logout).setVisible(false);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onResume() {
        super.onResume();
        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .build();
        imageLoader.displayImage(getFormedUrl(), imgEmployer, options, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                showProgress();
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                hideProgress();
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                hideProgress();
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                hideProgress();
            }
        });
    }

    private String getFormedUrl() {
        return String.format("%s?login=%s&password=%s&id=%d", ApiDataService.PICTURE_URL,
                PreferencesHelper.Instance.restoreUsername(),
                PreferencesHelper.Instance.restorePassword(),
                employer.getId()
        );
    }

    private void onPhoneClick(String phone) {
        if (phone.equals("null"))
            return;
        Uri call = Uri.parse("tel:" + phone);
        Intent intent = new Intent(Intent.ACTION_DIAL, call);
        if (intent.resolveActivity(getContext().getPackageManager()) != null)
            getActivity().startActivity(intent);
    }

    private void onEmailClick(String email) {
        if (email.equals("null"))
            return;
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/html");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
        if (intent.resolveActivity(getContext().getPackageManager()) != null)
            getActivity().startActivity(intent);
    }


    @Override
    void setupActionbar() {
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(employer.getName());
    }
}
