package ru.test.taxsee.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import ru.test.taxsee.R;
import ru.test.taxsee.models.ServerResponse;
import ru.test.taxsee.services.ApiDataService;
import ru.test.taxsee.tools.PreferencesHelper;
import ru.test.taxsee.tools.Tools;
import rx.Subscriber;

/**
 * Created by Dashkevich Makar
 * mcrena@mail.ru  on 08.03.17.
 */

public class AuthorizationFragment extends BaseFragment {
    private EditText edtLogin;
    private EditText edtPassword;
    private TextInputLayout loginWrapper, passWrapper;
    private String login;
    private String password;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.auth_frg, null);
        edtLogin = (EditText) v.findViewById(R.id.edtLogin);
        edtPassword = (EditText) v.findViewById(R.id.edtPassword);
        loginWrapper = (TextInputLayout) v.findViewById(R.id.loginWrapper);
        passWrapper = (TextInputLayout) v.findViewById(R.id.passwordWrapper);
        v.findViewById(R.id.btnEnter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkFields())
                    auth();
            }
        });
        return v;
    }

    private void auth() {
        showProgress();
        addSubscription(ApiDataService.Instance.auth(edtLogin.getText().toString(), edtPassword.getText().toString()).subscribe(new Subscriber<ServerResponse>() {
            @Override
            public void onCompleted() {
                hideProgress();
            }

            @Override
            public void onError(Throwable e) {
                hideProgress();
                handleError(e.getMessage());
            }

            @Override
            public void onNext(ServerResponse response) {
                if (!response.getSuccess())
                    handleError(response.getMessage());
                else {
                    PreferencesHelper.Instance.storeUsername(login);
                    PreferencesHelper.Instance.storePassword(password);
                    Tools.hideKeyboard(getActivity());
                    activity.nextFragment(new MainListFragment(), null, true);
                }
            }
        }));
    }


    @Override
    void setupActionbar() {
        actionBar.setDisplayShowCustomEnabled(false);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setTitle(R.string.auth);
    }

    private boolean checkFields() {
        loginWrapper.setErrorEnabled(false);
        passWrapper.setErrorEnabled(false);
        if (edtLogin.getText().length() == 0) {
            edtLogin.requestFocus();
            loginWrapper.setError(getString(R.string.fill_field));
            return false;
        }
        if (edtPassword.getText().length() == 0) {
            edtPassword.requestFocus();
            passWrapper.setError(getString(R.string.fill_field));
            return false;
        }
        login = edtLogin.getText().toString();
        password = edtPassword.getText().toString();
        return true;
    }
}
