package ru.test.taxsee.main;

import android.app.Application;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import ru.test.taxsee.services.ApiDataService;
import ru.test.taxsee.tools.PreferencesHelper;

/**
 * Created by Dashkevich Makar
 * mcrena@mail.ru  on 08.03.17.
 */

public class TaxseeApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        ApiDataService.Instance.init();
        PreferencesHelper.Instance.init(this);
        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(this));
    }
}
