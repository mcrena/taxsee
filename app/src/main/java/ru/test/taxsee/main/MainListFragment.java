package ru.test.taxsee.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import ru.test.taxsee.R;
import ru.test.taxsee.models.Department;
import ru.test.taxsee.models.Employer;
import ru.test.taxsee.services.ApiDataService;
import rx.Subscriber;

/**
 * Created by Dashkevich Makar
 * mcrena@mail.ru  on 08.03.17.
 */

public class MainListFragment extends BaseFragment {
    private ViewGroup root;
    private Department structure;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.main_list_frg, null);
        root = (ViewGroup) v.findViewById(R.id.root);
        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu, menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public void onResume() {
        super.onResume();
        if (structure == null) {
            showProgress();
            addSubscription(ApiDataService.Instance.getStructure().subscribe(new Subscriber<Department>() {
                @Override
                public void onCompleted() {
                    hideProgress();
                }

                @Override
                public void onError(Throwable e) {
                    hideProgress();
                    handleError(e.getMessage());
                }

                @Override
                public void onNext(Department department) {
                    structure = department;
                    fillList();
                }
            }));
        } else {
            fillList();
        }
    }

    private int paddingOffset = 20;

    private void fillList() {
        root.addView(getDepartmentView(structure, paddingOffset));
    }

    private View getDepartmentView(final Department department, int padding) {
        View departmentView = LayoutInflater.from(getContext()).inflate(R.layout.department_lisitem, null);
        ViewGroup departmentContainer = (ViewGroup) departmentView.findViewById(R.id.departmentContainer);
        ViewGroup departmentsContainer = (ViewGroup) departmentView.findViewById(R.id.departmentsContainer);
        ViewGroup employeesContainer = (ViewGroup) departmentView.findViewById(R.id.employeesContainer);
        View titleContainer = departmentView.findViewById(R.id.titleContainer);
        final ImageView imgExpand = (ImageView) departmentView.findViewById(R.id.imgExpandIndicator);
        imgExpand.setImageResource(department.expanded ? R.drawable.ic_arrow_up : R.drawable.ic_arrow_down);
        TextView departmentTitle = (TextView) departmentView.findViewById(R.id.txtDepartmentName);
        departmentTitle.setText(department.getName());
        titleContainer.setPadding(padding, 0, 20, 0);
        departmentContainer.setVisibility(department.expanded ? View.VISIBLE : View.GONE);
        departmentView.setTag(department);
        departmentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Department d = (Department) view.getTag();
                d.expanded = !d.expanded;
                view.findViewById(R.id.departmentContainer).setVisibility(d.expanded ? View.VISIBLE : View.GONE);
                imgExpand.setImageResource(d.expanded ? R.drawable.ic_arrow_up : R.drawable.ic_arrow_down);
            }
        });
        if (department.getDepartments() != null) {
            for (Department childDepartment : department.getDepartments()) {
                departmentContainer.addView(getDepartmentView(childDepartment, padding + paddingOffset));
            }
        } else {
            departmentsContainer.setVisibility(View.GONE);
        }

        if (department.getEmployees() != null) {
            for (Employer employer : department.getEmployees()) {
                employeesContainer.addView(getEmployerView(employer));
            }
        } else {
            employeesContainer.setVisibility(View.GONE);
        }


        return departmentView;
    }

    private View getEmployerView(Employer employer) {
        View employerView = LayoutInflater.from(getContext()).inflate(R.layout.employer_listitem, null);
        employerView.setTag(employer);
        employerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Employer e = (Employer) view.getTag();
                Bundle bundle = new Bundle();
                bundle.putSerializable("employer", e);
                activity.nextFragment(new EmployerFragment(), bundle, true);
            }
        });
        TextView txtName = (TextView) employerView.findViewById(R.id.txtEmployerName);
        TextView txtTitle = (TextView) employerView.findViewById(R.id.txtEmployerTitle);
        txtName.setText(employer.getName());
        txtTitle.setText(employer.getTitle());
        return employerView;
    }

    @Override
    void setupActionbar() {
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setTitle(getString(R.string.company));
    }
}
