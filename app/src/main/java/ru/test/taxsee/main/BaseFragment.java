package ru.test.taxsee.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import ru.test.taxsee.R;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Dashkevich Makar
 * mcrena@mail.ru  on 08.03.17.
 */

abstract public class BaseFragment extends Fragment {

    private ProgressBar progressBar;
    protected ActionBar actionBar;
    protected MainActivity activity;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        progressBar = (ProgressBar) getActivity().findViewById(R.id.progress);
        activity = (MainActivity) getActivity();
        actionBar = activity.getSupportActionBar();
        setupActionbar();

    }

    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    abstract void setupActionbar();

    private Subscription subscription;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    protected void addSubscription(Subscription newSubscription) {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
        subscription = newSubscription;
        compositeSubscription.add(subscription);
    }

    protected void handleError(String error) {
        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (!compositeSubscription.isUnsubscribed())
            compositeSubscription.unsubscribe();
    }
}
