package ru.test.taxsee.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import ru.test.taxsee.R;
import ru.test.taxsee.tools.PreferencesHelper;

/**
 * Created by Dashkevich Makar
 * mcrena@mail.ru  on 08.03.17.
 */

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        Toolbar toolbar = (Toolbar) findViewById(ru.test.taxsee.R.id.toolbar);
        setSupportActionBar(toolbar);
        if (savedInstanceState == null)
            startFragment();
    }

    private void startFragment() {
        if (PreferencesHelper.Instance.restoreUsername() != null)
            nextFragment(new MainListFragment(), null, true);
        else
            nextFragment(new AuthorizationFragment(), null, true);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    public void nextFragment(BaseFragment fragment, Bundle bundle, boolean addToBackStack) {
        findViewById(R.id.progress).setVisibility(View.GONE);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        fragment.setArguments(bundle);
        ft.replace(R.id.content_frame, fragment, fragment.getClass().getName());
        if (addToBackStack)
            ft.addToBackStack(null);
        ft.commitAllowingStateLoss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.logout:
                while (getSupportFragmentManager().getBackStackEntryCount() != 0)
                    getSupportFragmentManager().popBackStackImmediate();
                PreferencesHelper.Instance.storePassword(null);
                PreferencesHelper.Instance.storeUsername(null);
                nextFragment(new AuthorizationFragment(), null, true);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        findViewById(R.id.progress).setVisibility(View.GONE);
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        } else {
            super.onBackPressed();
        }

    }
}
