package ru.test.taxsee.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.test.taxsee.models.Department;
import ru.test.taxsee.models.ServerResponse;
import ru.test.taxsee.tools.PreferencesHelper;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Dashkevich Makar
 * mcrena@mail.ru  on 08.03.17.
 */

public enum ApiDataService {
    Instance;
    public static final String BASE_URL = "https://contact.taxsee.com";
    public static final String PICTURE_URL = BASE_URL + "/Contacts.svc/GetWPhoto";
    private ApiService apiService;

    private ApiDataService() {

    }


    public void init() {
        RxJavaCallAdapterFactory rxAdapter = RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io());
        final OkHttpClient.Builder builder = new OkHttpClient().newBuilder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder.addInterceptor(interceptor);

        OkHttpClient client = builder.build();


        Gson gson = new GsonBuilder().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(rxAdapter)
                .client(client)
                .build();

        apiService = retrofit.create(ApiService.class);
    }

    public Observable<ServerResponse> auth(String login, String password) {
        return apiService.auth(login, password).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Department> getStructure() {
        return apiService.getStructure(PreferencesHelper.Instance.restoreUsername(), PreferencesHelper.Instance.restorePassword())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
