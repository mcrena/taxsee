package ru.test.taxsee.services;

import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.test.taxsee.models.Department;
import ru.test.taxsee.models.ServerResponse;
import rx.Observable;

/**
 * Created by Dashkevich Makar
 * mcrena@mail.ru  on 08.03.17.
 */

public interface ApiService {

    @GET("/Contacts.svc/Hello")
    Observable<ServerResponse> auth(@Query("login") String login, @Query("password") String password);

    @GET("/Contacts.svc/GetAll")
    Observable<Department> getStructure(@Query("login") String login, @Query("password") String password);
}
