package ru.test.taxsee.tools;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Dashkevich Makar
 * mcrena@mail.ru  on 08.03.17.
 */

public enum PreferencesHelper {
    Instance;

    private PreferencesHelper() {

    }

    private SharedPreferences preferences;

    public void init(Context context) {
        preferences = context.getSharedPreferences("main_prefs", Context.MODE_PRIVATE);
    }

    public void storeUsername(String username) {
        preferences.edit().putString("username", username).commit();
    }

    public String restoreUsername() {
        return preferences.getString("username", null);
    }

    public void storePassword(String password) {
        preferences.edit().putString("password", password).commit();
    }

    public String restorePassword() {
        return preferences.getString("password", null);
    }


}
